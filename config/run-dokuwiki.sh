#!/bin/sh

if test "$DEBUG"; then
    set -x
fi
. /usr/local/bin/nsswrapper.sh

APACHE_HTTP_PORT=${APACHE_HTTP_PORT:-8080}
DOKU_DATA_DIR="${DOKU_DATA_DIR:-/usr/src/dokuwiki/data}"
DOKUWIKI_MANAGER="${DOKUWIKI_MANAGER:-@Admins}"
DOKUWIKI_REMOTEUSER="${DOKUWIKI_REMOTEUSER:-@Admins}"
DOKUWIKI_SUPERUSER="${DOKUWIKI_SUPERUSER:-@Admins}"
LLNG_PROTO=${LLNG_PROTO:-http}
OPENLDAP_BIND_DN_PREFIX="${OPENLDAP_BIND_DN_PREFIX:-cn=dokuwiki,ou=services}"
OPENLDAP_BIND_PW="${OPENLDAP_BIND_PW:-secret}"
OPENLDAP_CONF_DN_PREFIX="${OPENLDAP_CONF_DN_PREFIX:-ou=lemonldap,ou=config}"
OPENLDAP_DOMAIN=${OPENLDAP_DOMAIN:-demo.local}
OPENLDAP_HOST=${OPENLDAP_HOST:-}
OPENLDAP_PROTO=${OPENLDAP_PROTO:-ldap}
PUBLIC_PROTO=${PUBLIC_PROTO:-http}
if test -z "$OPENLDAP_BASE"; then
    OPENLDAP_BASE=`echo "dc=$OPENLDAP_DOMAIN" | sed 's|\.|,dc=|g'`
fi
if test -z "$OPENLDAP_PORT" -a "$OPENLDAP_PROTO" = ldaps; then
    OPENLDAP_PORT=636
elif test -z "$OPENLDAP_PORT"; then
    OPENLDAP_PORT=389
fi
if test -z "$APACHE_DOMAIN"; then
    APACHE_DOMAIN=wiki.$OPENLDAP_DOMAIN
fi
if test -z "$LLNG_SSO_DOMAIN"; then
    LLNG_SSO_DOMAIN=auth.$OPENLDAP_DOMAIN
fi
export APACHE_DOMAIN
export APACHE_HTTP_PORT
export OPENLDAP_BASE
export OPENLDAP_BIND_DN_PREFIX
export OPENLDAP_DOMAIN
export OPENLDAP_HOST
export PUBLIC_PROTO
SSL_INCLUDE=no-ssl
. /usr/local/bin/reset-tls.sh
export RESET_TLS=false

directory_empty()
{
    test -z "$(ls -A "$1/")"
}

if test "`id -u`" = 0; then
    rsync_options="-rlDog --chown www-data:root"
else
    rsync_options=-rlD
fi
if ! test -d /var/www/html/data; then
    rsync $rsync_options /usr/src/dokuwiki/ /var/www/html/
else
    should_upgrade=false
    vleft=`sed 's|-|.|g' /var/www/html/VERSION | awk '{print $1}'`
    vright=`sed 's|-|.|g' /usr/src/dokuwiki/VERSION | awk '{print $1}'`
    majl=$(echo $vleft | cut -d. -f1)
    majr=$(echo $vright | cut -d. -f1)
    minl=$(echo $vleft | cut -d. -f2)
    minr=$(echo $vright | cut -d. -f2)
    pl=$(echo $vleft | cut -d. -f3)
    pr=$(echo $vright | cut -d. -f3)
    if test "$majr" -gt "$majl"; then
	should_upgrade=true
    elif test "$majr" -eq "$majl"; then
	if test "$minr" -gt "$minl"; then
	    should_upgrade=true
	elif test "$minr" -eq "$minl"; then
	    if test "$pr" -a -z "$pl"; then
		should_upgrade=true
	    elif test "$pr" -a "$pl" -a "$pr" != "$pl"; then
		if ( echo "$pr" ; echo "$pl" ) | sort -n \
			| tail -1 | grep "$pr" >/dev/null; then
		    should_upgrade=true
		fi
	    fi
	fi
    fi
    if $should_upgrade; then
	rsync $rsync_options --delete --exclude /conf/ --exclude /data/ \
	    --exclude "*lib/plugins*" /usr/src/dokuwiki/ /var/www/html/
	for dir in conf data lib/plugins lib/tpl
	do
	    if [ ! -d "/var/www/html/$dir" ] || \
		directory_empty "/var/www/html/$dir"; then
		rsync $rsync_options --include "/$dir/" --exclude '/*' \
		    /usr/src/dokuwiki/ /var/www/html/
	    fi
	done
    fi
fi
if ! test -s /var/www/html/conf/dokuwiki.php; then
    sed -e "s|DK_MANAGER|$DOKUWIKI_MANAGER|g" \
        -e "s|DK_REMOTEUSER|$DOKUWIKI_REMOTEUSER|g" \
	-e "s|DK_SUPERUSER|$DOKUWIKI_SUPERUSER|g" \
	/dokuwiki.php >/var/www/html/conf/dokuwiki.php
    echo INFO: DokuWiki configured
fi
if test "$SSO_LOCKOUT"; then
    export AUTH_METHOD=lemon
    if ! grep "\$plugins\['authlemonldap'\].*=.*1.*;" \
	/var/www/html/conf/plugins*php >/dev/null 2>/dev/null; then
	cat <<EOF >>/var/www/html/conf/plugins.required.php
\$plugins['authlemonldap'] = 1;
EOF
	echo INFO: AuthLemonLDAP plugin loaded
    fi
    if grep authplain /var/www/html/conf/dokuwiki.php >/dev/null 2>&1; then
	sed -i 's|authplain|authlemonldap|' /var/www/html/conf/dokuwiki.php
	echo INFO: DokuWiki AuthType set to LemonLDAP
    fi
elif test -z "$OPENLDAP_HOST"; then
    export APACHE_IGNORE_OPENLDAP=yay
    rm -f /etc/apache2/mods-enabled/perl.load
fi

if test -s /var/www/html/conf/acl.auth.php.dist; then
    echo "Enabling ACL sample configuration"
    mv /var/www/html/conf/acl.auth.php.dist /var/www/html/conf/acl.auth.php
fi

echo "Install DokuWiki Site Configuration"
sed -e "s HTTP_HOST $APACHE_DOMAIN g" \
    -e "s HTTP_PORT $APACHE_HTTP_PORT g" \
    -e "s SSL_CONF /etc/apache2/$SSL_INCLUDE.conf g" \
    /vhost.conf >/etc/apache2/sites-enabled/003-vhosts.conf

. /run-apache.sh
