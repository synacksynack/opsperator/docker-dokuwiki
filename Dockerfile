FROM opsperator/php

# DokuWiki image for OpenShift Origin

ARG DO_UPGRADE=
ENV DK_URL=http://download.dokuwiki.org/src/dokuwiki \
    DK_VERSION=2022-07-31a

LABEL io.k8s.description="DokuWiki is a simple to use and highly versatile Open Source wiki software" \
      io.k8s.display-name="DokuWiki $DK_VERSION" \
      io.openshift.expose-services="8080:http" \
      io.openshift.tags="dokuwiki,wiki" \
      io.openshift.non-scalable="true" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-dokuwiki" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="$DK_VERSION"

USER root

COPY config/* /

RUN set -x \
    && rm -rf /var/lib/apt/lists/* \
    && mkdir -p /usr/share/man/man1 /usr/share/man/man7 /usr/src/dokuwiki \
    && apt-get -y update \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install DokuWiki Dependencies" \
    && apt-get -y --no-install-recommends install rsync bzip2 busybox-static \
	ldap-utils openssl wget libpng16-16 libjpeg62-turbo libwebp6 libgd3 \
    && savedAptMark="$(apt-mark showmanual)" \
    && apt-get -y --no-install-recommends install libcurl4-openssl-dev \
	libfreetype6-dev libicu-dev libjpeg-dev libldap2-dev libmcrypt-dev \
	libxml2-dev libevent-dev libonig-dev libwebp-dev \
    && debMultiarch="$(dpkg-architecture --query DEB_BUILD_MULTIARCH)" \
    && docker-php-ext-configure gd --with-freetype --with-jpeg --with-webp \
	--with-png \
    && docker-php-ext-configure ldap --with-libdir="lib/$debMultiarch" \
    && docker-php-ext-install curl exif gd intl ldap mbstring opcache pcntl \
    && echo "# Install DokuWiki" \
    && curl -fsSL -o dokuwiki.tar.gz "$DK_URL/dokuwiki-$DK_VERSION.tgz" \
    && tar -xzf dokuwiki.tar.gz -C /usr/src/dokuwiki --strip-components 1 \
    && echo "# Configure DokuWiki" \
    && mv /opcache.ini /usr/local/etc/php/conf.d/opcache-recommended.ini \
    && mv /memory-limit.ini /usr/local/etc/php/conf.d/memory-limit.ini \
    && echo "# Fixing Permissions" \
    && chown -R 1001:root /usr/src/dokuwiki /var/www/html \
    && chmod -R g=u /usr/src/dokuwiki /var/www/html \
    && echo "# Cleaning up" \
    && apt-mark auto '.*' >/dev/null \
    && apt-mark manual $savedAptMark \
    && ldd "$(php -r 'echo ini_get("extension_dir");')"/*.so \
	| awk '/=>/ { print $3 }' | sort -u | xargs -r dpkg-query -S \
	| cut -d: -f1 | sort -u | xargs -rt apt-mark manual \
    && apt-get purge -y --auto-remove \
	-o APT::AutoRemove::RecommendsImportant=false \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man \
	dokuwiki.tar.gz /usr/src/php.tar.xz \
	/usr/src/dokuwiki/conf/dokuwiki.php \
    && unset HTTP_PROXY HTTPS_PROXY NO_PROXY DO_UPGRADE http_proxy https_proxy

ADD authlemonldap /usr/src/dokuwiki/lib/plugins/authlemonldap/
CMD "/usr/sbin/apache2ctl" "-D" "FOREGROUND"
ENTRYPOINT ["dumb-init","--","/run-dokuwiki.sh"]
USER 1001
