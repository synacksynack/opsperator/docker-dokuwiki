# k8s DokuWiki

DokuWiki image.

Integrating https://github.com/neffs/dokuwiki-plugin-authlemonldap setting up
LLNG authentication.

Build with:

```
$ make build
```

Depends on LDAP & LLNG.

Start Demo or Cluster in OpenShift:

```
$ make ocdemo
$ make ocprod
```

Cleanup OpenShift assets:

```
$ make ocpurge
```

Environment variables and volumes
----------------------------------

The image recognizes the following environment variables that you can set during
initialization by passing `-e VAR=VALUE` to the Docker `run` command.

|    Variable name           |    Description            | Default                                                     | Inherited From    |
| :------------------------- | ------------------------- | ----------------------------------------------------------- | ----------------- |
|  `APACHE_IGNORE_OPENLDAP`  | Ignore LemonLDAP autoconf | undef                                                       | opsperator/apache |
|  `APACHE_DOMAIN`           | DokuWiki ServerName       | hostname or `localhost`                                     | opsperator/apache |
|  `APACHE_HTTP_PORT`        | DokuWiki HTTP(s) Port     | `8080`                                                      | opsperator/apache |
|  `LLNG_PROTO`              | LemonLDAP-NG public proto | `http`                                                      |                   |
|  `LLNG_SSO_DOMAIN`         | LemonLDAP-NG address      | `auth.${OPENLDAP_DOMAIN}`                                   |                   |
|  `OPENLDAP_BASE`           | OpenLDAP Base             | seds `OPENLDAP_DOMAIN`, default produces `dc=demo,dc=local` | opsperator/apache |
|  `OPENLDAP_BIND_DN_RREFIX` | OpenLDAP Bind DN Prefix   | `cn=dokuwiki,ou=services`                                   | opsperator/apache |
|  `OPENLDAP_BIND_PW`        | OpenLDAP Bind Password    | `secret`                                                    | opsperator/apache |
|  `OPENLDAP_CONF_DN_RREFIX` | OpenLDAP Conf DN Prefix   | `cn=lemonldap,ou=config`                                    | opsperator/apache |
|  `OPENLDAP_DOMAIN`         | OpenLDAP Domain Name      | `demo.local`                                                | opsperator/apache |
|  `OPENLDAP_HOST`           | OpenLDAP Backend Address  | undef                                                       | opsperator/apache |
|  `OPENLDAP_PORT`           | OpenLDAP Bind Port        | `389` or `636` depending on `OPENLDAP_PROTO`                | opsperator/apache |
|  `OPENLDAP_PROTO`          | OpenLDAP Proto            | `ldap`                                                      | opsperator/apache |
|  `PHP_ERRORS_LOG`          | PHP Errors Logs Output    | `/proc/self/fd/2`                                           | opsperator/php    |
|  `PHP_MAX_EXECUTION_TIME`  | PHP Max Execution Time    | `30` seconds                                                | opsperator/php    |
|  `PHP_MAX_FILE_UPLOADS`    | PHP Max File Uploads      | `20`                                                        | opsperator/php    |
|  `PHP_MAX_POST_SIZE`       | PHP Max Post Size         | `8M`                                                        | opsperator/php    |
|  `PHP_MAX_UPLOAD_FILESIZE` | PHP Max Upload File Size  | `2M`                                                        | opsperator/php    |
|  `PHP_MEMORY_LIMIT`        | PHP Memory Limit          | `-1` (no limitation)                                        | opsperator/php    |
|  `PUBLIC_PROTO`            | DokuWiki HTTP Proto       | `http`                                                      | opsperator/apache |
|  `SSO_LOCKOUT`             | DokuWiki SSO Lockout      | undef                                                       |                   |


You can also set the following mount points by passing the `-v /host:/container`
flag to Docker.

|  Volume mount point | Description                     | Inherited From    |
| :------------------ | ------------------------------- | ----------------- |
|  `/certs`           | Apache Certificate (optional)   | opsperator/apache |
|  `/var/www/html`    | DokuWiki site root              |                   |
